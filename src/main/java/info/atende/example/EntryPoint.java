package info.atende.exemple;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 * Criado por Giovanni Silva <giovanni@atende.info>
 * Date: 10/7/13
 * Time: 10:55 PM
 */
@ServerEndpoint("/hello")
public class EntryPoint {
    @OnMessage
    public String handleMessage(String message){
        return "Here is your message " + message;
    }
    @OnClose
    public void handleClose(){
        System.out.println("Closed");
    }
    @OnOpen
    public void open(Session session){
        System.out.println("Open Connection with " + session.getRequestURI());
    }
}
