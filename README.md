JAVA EE 7 INITIAL SETUP
============================

## Introdution ##

This is a simple project to be forked and is a initial point to Java EE 7 Projects. This project use JBoss Wildfly and Arquillian. Other servers are provided using maven profiles.

## Getting Started ##

With **Jboss Wildfly** You need to setup the variable JBOSS\_HOME with a value pointing to your instalation. You can do that in the Intellij IDEA for Run Tests, or you can set this variable in your environment (so you can run maven and IDE with no problems)

This setup come with a simple WebSockts example.
